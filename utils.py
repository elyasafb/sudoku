def get_sub_square_cells(variables, square):
    sub_square_cells = filter(lambda x: square in x, variables)
    return list(sub_square_cells)


def get_rows_cells(cell):
    square = cell[0]
    cell_pos = int(cell[1])
    idx = int(cell_pos / 3)
    cells_relevant_pos = [str(i) for i in range(idx * 3, (idx + 1) * 3)]
    relevant_sub_square = sub_square_to_rows(square)
    res = []
    for sqr in relevant_sub_square:
        for cell_pos in cells_relevant_pos:
            res.append(sqr + cell_pos)
    return res


def sub_square_to_rows(square):
    if square in ['A', 'B', 'C']:
        return ['A', 'B', 'C']
    if square in ['D', 'E', 'F']:
        return ['D', 'E', 'F']
    if square in ['G', 'H', 'I']:
        return ['G', 'H', 'I']


def sub_square_to_cols(square):
    if square in ['A', 'D', 'G']:
        return ['A', 'D', 'G']
    if square in ['B', 'E', 'H']:
        return ['B', 'E', 'H']
    if square in ['C', 'F', 'I']:
        return ['C', 'F', 'I']


def get_col_cells(cell):
    square = cell[0]
    cell_pos = int(cell[1])
    idx = int(cell_pos % 3)
    cells_relevant_pos = [str(i * 3 + idx) for i in range(3)]
    relevant_sub_square = sub_square_to_cols(square)
    res = []
    for sqr in relevant_sub_square:
        for cell_pos in cells_relevant_pos:
            res.append(sqr + cell_pos)
    return res


