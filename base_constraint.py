# Base class for all constraints
from abc import ABC, abstractmethod
from typing import Generic, List, Dict, TypeVar

V = TypeVar('V')  # variable type
D = TypeVar('D')  # domain type


class Constraint(Generic[V, D], ABC):
    # The variables that the constraint is between
    def __init__(self, variables: List[V]) -> None:
        self.variables = variables

    @abstractmethod
    def satisfied(self, assignment: Dict[V, D]) -> bool:
        pass
