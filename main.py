from typing import Optional, Dict, List

from csp import CSP
from utils import get_sub_square_cells, get_rows_cells, get_col_cells
from sudoku_constarints import SudokuConstrains


def add_sub_square_constraint(csp):
    # constraint on sub square
    for square in sub_square.values():
        sub_square_cells = get_sub_square_cells(variables, square)
        for i in range(len(sub_square_cells) - 1):
            for j in range(i + 1, len(sub_square_cells)):
                csp.add_constraint(SudokuConstrains(sub_square_cells[i], sub_square_cells[j]))


def add_row_constraint(csp):
    # constraint on rows
    for cell in variables:
        if cell[0] in ['A', 'D', 'G'] and cell[1] in ['0', '3', '6']:
            rows_cells = get_rows_cells(cell)
            for i in range(len(rows_cells) - 1):
                for j in range(i + 1, len(rows_cells)):
                    csp.add_constraint(SudokuConstrains(rows_cells[i], rows_cells[j]))


def add_col_constraint(csp):
    #  constraint on columns
    for cell in variables:
        if cell[0] in ['A', 'B', 'C'] and cell[1] in ['0', '1', '2']:
            col_cell = get_col_cells(cell)
            for i in range(len(col_cell) - 1):
                for j in range(i + 1, len(col_cell)):
                    csp.add_constraint(SudokuConstrains(col_cell[i], col_cell[j]))


def print_solution(solution):
    cnt = 0
    rows = [[] for i in range(9)]
    i = 0
    for key in sorted(solution.keys()):
        rows[i].append(key)
        cnt += 1
        if cnt % 3 == 0:
            i += 1
            i = i % 3 + int(cnt / 27) * 3

    ordered_cells = [item for row in rows for item in row]
    for key in ordered_cells:
        value = solution[key]
        if cnt % 3 == 0:
            print('|', end=' ')
        print(value, end='  ')
        cnt += 1
        if cnt % 9 == 0:
            print('|')
        if cnt % 27 == 0:
            print(' - ' * 12)


def solve_game(csp, initial_game):
    solution: Optional[Dict[str, str]] = csp.backtracking_search(initial_game)
    if solution is None:
        print("No solution found!")
    else:
        print_solution(solution)


if __name__ == "__main__":
    sub_square: Dict[int, str] = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G',
                                  7: 'H', 8: 'I'}
    variables: List[str] = [square + str(i) for square in sub_square.values() for i in range(9)]
    domains: Dict[str, List[int]] = {}
    for variable in variables:
        domains[variable] = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    csp: CSP[str, int] = CSP(variables, domains)
    add_sub_square_constraint(csp)
    add_col_constraint(csp)
    add_row_constraint(csp)
    initial_game = {'A5': 2, 'C7': 4, 'A8': 9, 'B5': 4, 'B1': 5, 'B2': 6, 'B3': 7, 'B4': 8}
    solve_game(csp, initial_game)
