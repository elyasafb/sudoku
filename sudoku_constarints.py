from typing import Dict

from csp import Constraint, V, D


class SudokuConstrains(Constraint[str, str]):
    def __init__(self, cell1: str, cell2: str) -> None:
        super().__init__([cell1, cell2])
        self.cell1: str = cell1
        self.cell2: str = cell2

    def satisfied(self, assignment: Dict[V, D]) -> bool:
        if self.cell1 not in assignment or self.cell2 not in assignment:
            return True

        return assignment[self.cell1] != assignment[self.cell2]
